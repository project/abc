<?php
/**
 * Implements hook_form_system_theme_settings_alter() function.
 *
 * @param $form
 *   Nested array of form elements that comprise the form.
 * @param $form_state
 *   A keyed array containing the current state of the form.
 */
function abc_form_system_theme_settings_alter(&$form, &$form_state) {
// Work-around for a core bug affecting admin themes.
  if (isset($form_id)) {
    return;
  }

// Create the form using Forms API

// ### Logo Settings ###
  $form['logo']['logoabc'] = array(
    '#type' => 'select',
    '#description' => t('Choose the default letter as logo.'),
    '#default_value' => theme_get_setting('logoabc'),
    '#options' => array(
      'abc' => t('ABC generic logo'),
      'a' => 'A',
      'b' => 'B',
      'c' => 'C',
      'd' => 'D',
      'e' => 'E',
      'f' => 'F',
      'g' => 'G',
      'h' => 'H',
      'i' => 'I',
      'j' => 'J',
      'k' => 'K',
      'l' => 'L',
      'm' => 'M',
      'n' => 'N',
      'o' => 'O',
      'p' => 'P',
      'q' => 'Q',
      'r' => 'R',
      's' => 'S',
      't' => 'T',
      'u' => 'U',
      'v' => 'V',
      'w' => 'W',
      'x' => 'X',
      'y' => 'Y',
      'z' => 'Z',
    ),
  );
  $form['logo']['logocol'] = array(
    '#type' => 'select',
    '#description' => t('Choose the default logo color.'),
    '#default_value' => theme_get_setting('logocol'),
    '#options' => array(
      'pink'   => t('Pink Logo'),
      'green'  => t('Green Logo'),
      'orange' => t('Orange Logo'),
      'blue'   => t('Blue Logo'),
      'grey'   => t('Grey Logo'),
    ),
  );

// ### Pure.CSS settings ###
  $form['puregrid'] = array(
    '#type' => 'fieldset',
    '#title' => t('Pure Grid settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['puregrid']['css_zone'] = array(
    '#type' => 'checkbox',
    '#title' => t('Yahoo Pure.CSS Framework CDN'),
    '#description' => t('Select this option to load Yahoo Pure.CSS Framework files from CDN. If you encounter issues or prefer to load Pure.CSS files locally, leave this option unchecked.'),
    '#default_value' => theme_get_setting('css_zone')
  );
  $form['puregrid']['wrapper'] = array(
    '#type' => 'textfield',
    '#title' => t('Layout width'),
    '#description' => t('Specify the width of the layout using <b>em</b> values if possible (e.g., 90em), px values (e.g., 1200px), or percentages. Alternatively, leave it empty or set it to 100% for a fluid layout.'),
    '#default_value' => theme_get_setting('wrapper'),
    '#size' => 10,
  );
  $form['puregrid']['first_width'] = array(
    '#type' => 'select',
    '#title' => t('First (left) sidebar width'),
    '#description' => t('Set the width of the first (left) sidebar.'),
    '#default_value' => theme_get_setting('first_width'),
    '#options' => array(
      3 => t('narrower'),
      4 => t('narrow'),
      5 => t('NORMAL'),
      6 => t('wide'),
      7 => t('wider'),
    ),
  );
  $form['puregrid']['second_width'] = array(
    '#type' => 'select',
    '#title' => t('Second (right) sidebar width'),
    '#description' => t('Set the width of the second (right) sidebar.'),
    '#default_value' => theme_get_setting('second_width'),
    '#options' => array(
      3 => t('narrower'),
      4 => t('narrow'),
      5 => t('NORMAL'),
      6 => t('wide'),
      7 => t('wider'),
    ),
  );
  $form['puregrid']['mobile_blocks'] = array(
    '#type' => 'select',
    '#title' => t('Hide blocks on mobile devices'),
    '#description' => t('If you have numerous blocks, consider hiding some of them when viewed on mobile devices.'),
    '#default_value' => theme_get_setting('mobile_blocks'),
    '#options' => array(
      0 => t('Show all blocks'),
      1 => t('Hide blocks on user regions 1-4'),
      2 => t('Hide blocks on user regions 1-4 and left sidebar'),
      3 => t('Hide blocks on all user regions'),
      4 => t('Hide blocks on all user regions and left sidebar'),
      5 => t('Hide blocks on all user regions and both sidebars'),
    ),
  );

// ### Layout Settings ###
  $form['layout_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Layout settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

// Background Settings
  $form['layout_settings']['background_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Background settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['layout_settings']['background_settings']['bgimg'] = array(
    '#type' => 'checkbox',
    '#title' => t('Image'),
    '#default_value' => theme_get_setting('bgimg'),
  );
  $form['layout_settings']['background_settings']['bgcol'] = array(
    '#type' => 'select',
    '#title' => t('Color'),
    '#default_value' => theme_get_setting('bgcol'),
    '#options' => array(
      0 => t('None (custom)'),
      1 => t('Pink'),
      2 => t('Green'),
      3 => t('Yellow'),
      4 => t('Blue'),
      5 => t('Black & White'),
    )
  );
  $form['layout_settings']['background_settings']['bgpoz'] = array(
    '#type' => 'select',
    '#title' => t('Position'),
    '#default_value' => theme_get_setting('bgpoz'),
    '#options' => array(
      0 => t('Static'),
      1 => t('Fixed'),
    )
  );

// Title settings
  $form['layout_settings']['title_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Title settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['layout_settings']['title_settings']['sncol'] = array(
    '#type' => 'select',
    '#title' => t('Site Name Color'),
    '#default_value' => theme_get_setting('sncol'),
    '#options' => array(
      0 => t('None (custom)'),
      1 => t('Pink'),
      2 => t('Green'),
      3 => t('Orange'),
      4 => t('Blue'),
      5 => t('Grey'),
    )
  );
  $form['layout_settings']['title_settings']['ntcol'] = array(
    '#type' => 'select',
    '#title' => t('Node Title Color'),
    '#default_value' => theme_get_setting('ntcol'),
    '#options' => array(
      0 => t('None (custom)'),
      1 => t('Pink'),
      2 => t('Green'),
      3 => t('Orange'),
      4 => t('Blue'),
      5 => t('Grey'),
    )
  );

// Block settings
  $form['layout_settings']['block_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Block settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['layout_settings']['block_settings']['btcol'] = array(
    '#type' => 'select',
    '#title' => t('Block Title Color'),
    '#default_value' => theme_get_setting('btcol'),
    '#options' => array(
      0 => t('None (custom)'),
      1 => t('Pink'),
      2 => t('Green'),
      3 => t('Orange'),
      4 => t('Blue'),
      5 => t('Grey'),
      6 => t('Multicolor'),
    )
  );
  $form['layout_settings']['block_settings']['blockicons'] = array(
    '#type' => 'select',
    '#title' => t('Block icons'),
    '#default_value' => theme_get_setting('blockicons'),
    '#options' => array(
      0 => t('No'),
      1 => t('Yes'),
    )
  );
  $form['layout_settings']['block_settings']['themedblocks'] = array(
    '#type' => 'select',
    '#title' => t('Themed blocks'),
    '#default_value' => theme_get_setting('themedblocks'),
    '#options' => array(
      0 => t('Sidebars only'),
      1 => t('Sidebars + User regions'),
      2 => t('User regions only'),
      3 => t('None'),
    )
  );

// Menu style and colous
  $form['layout_settings']['menu_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Menu settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['layout_settings']['menu_settings']['navpos'] = array(
    '#type' => 'select',
    '#title' => t('Main and secondary menus position'),
    '#default_value' => theme_get_setting('navpos'),
    '#options' => array(
      0 => t('Left'),
      1 => t('Center'),
      2 => t('Right'),
    )
  );
  $form['layout_settings']['menu_settings']['dropdown'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show Dropdown'),
    '#default_value' => theme_get_setting('dropdown'),
  );
  $form['layout_settings']['menu_settings']['menu_colors'] = array(
    '#type' => 'fieldset',
    '#title' => t('Menu item colors'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['layout_settings']['menu_settings']['menu_colors']['fbcol'] = array(
    '#type' => 'select',
    '#title' => t('First item'),
    '#default_value' => theme_get_setting('fbcol'),
    '#options' => array(
      0 => t('None (custom)'),
      1 => t('Pink'),
      2 => t('Green'),
      3 => t('Orange'),
      4 => t('Blue'),
      5 => t('Dark Blue'),
      6 => t('Light Grey'),
      7 => t('Grey'),
      8 => t('Dark Grey'),
      9 => t('Black'),
    )
  );
  $form['layout_settings']['menu_settings']['menu_colors']['mbcol'] = array(
    '#type' => 'select',
    '#title' => t('Menu items'),
    '#default_value' => theme_get_setting('mbcol'),
    '#options' => array(
      0 => t('None (custom)'),
      1 => t('Pink'),
      2 => t('Green'),
      3 => t('Orange'),
      4 => t('Blue'),
      5 => t('Dark Blue'),
      6 => t('Light Grey'),
      7 => t('Grey'),
      8 => t('Dark Grey'),
      9 => t('Black'),
    )
  );
  $form['layout_settings']['menu_settings']['menu_colors']['lbcol'] = array(
    '#type' => 'select',
    '#title' => t('Last item'),
    '#default_value' => theme_get_setting('lbcol'),
    '#options' => array(
      0 => t('None (custom)'),
      1 => t('Pink'),
      2 => t('Green'),
      3 => t('Orange'),
      4 => t('Blue'),
      5 => t('Dark Blue'),
      6 => t('Light Grey'),
      7 => t('Grey'),
      8 => t('Dark Grey'),
      9 => t('Black'),
    )
  );
  $form['layout_settings']['menu_settings']['menu_colors']['mrcol'] = array(
    '#type' => 'select',
    '#title' => t('Rollover'),
    '#default_value' => theme_get_setting('mrcol'),
    '#options' => array(
      0 => t('None (custom)'),
      1 => t('Pink'),
      2 => t('Green'),
      3 => t('Orange'),
      4 => t('Blue'),
      5 => t('Dark Blue'),
      6 => t('Light Grey'),
      7 => t('Grey'),
      8 => t('Dark Grey'),
      9 => t('Black'),
    )
  );
  $form['layout_settings']['menu_settings']['menu_colors']['trcol'] = array(
    '#type' => 'select',
    '#title' => t('Tertiary menu items color'),
    '#default_value' => theme_get_setting('trcol'),
    '#options' => array(
      0 => t('None (custom)'),
      1 => t('Pink'),
      2 => t('Green'),
      3 => t('Orange'),
      4 => t('Blue'),
      5 => t('Grey'),
    )
  );
  $form['layout_settings']['menu_settings']['menu2'] = array(
    '#type' => 'checkbox',
    '#title' => t('Duplicate the Main Menu at the bottom of the page.'),
    '#default_value' => theme_get_setting('menu2'),
  );

// the rest of layout settings
  $form['layout_settings']['fntfam'] = array(
    '#type' => 'select',
    '#title' => t('Font family'),
    '#default_value' => theme_get_setting('fntfam'),
    '#options' => array(
      0 => t('Sans-serif (Pure.CSS default)'),
      1 => t('Montserrat'),
      2 => t('Merriweather'),
    )
  );
  $form['layout_settings']['fntsize'] = array(
    '#type' => 'select',
    '#title' => t('Font size'),
    '#default_value' => theme_get_setting('fntsize'),
    '#options' => array(
      0 => t('Normal'),
      1 => t('Large'),
      2 => t('Larger'),
    )
  );
  $form['layout_settings']['searchimg'] = array(
    '#type' => 'select',
    '#title' => t('Search submit type'),
    '#description' => t('Search field will have a button, an image or none at all.'),
    '#default_value' => theme_get_setting('searchimg'),
    '#options' => array(
      0 => t('Button'),
      1 => t('Icon'),
      2 => t('None'),
    )
  );
  $form['layout_settings']['loginlinks'] = array(
    '#type' => 'checkbox',
    '#title' => t('Login/register links'),
    '#description' => t('Show login/register or user links.'),
    '#default_value' => theme_get_setting('loginlinks'),
  );
  $form['layout_settings']['pageicons'] = array(
    '#type' => 'checkbox',
    '#title' => t('Page icons'),
    '#default_value' => theme_get_setting('pageicons'),
  );
  $form['layout_settings']['roundcorners'] = array(
    '#type' => 'checkbox',
    '#title' => t('Rounded corners'),
    '#description' => t('Some page elements (comments, search, blocks) and main menu will have rounded corners.'),
    '#default_value' => theme_get_setting('roundcorners'),
  );
  $form['layout_settings']['devlink'] = array(
    '#type' => 'checkbox',
    '#title' => t('Developer link'),
    '#description' => t('Show/hide the link.'),
    '#default_value' => theme_get_setting('devlink'),
  );

// ### Breadcrumb ###
  $form['breadcrumb'] = array(
    '#type' => 'fieldset',
    '#title' => t('Breadcrumb'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['breadcrumb']['breadcrumb_display'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display breadcrumb'),
    '#default_value' => theme_get_setting('breadcrumb_display'),
  );

// ### Author & Date Settings ###
  $form['submitted_by'] = array(
    '#type' => 'fieldset',
    '#title' => t('What author/date information to display?'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['submitted_by']['postedby'] = array(
    '#type' => 'select',
    '#default_value' => theme_get_setting('postedby'),
    '#description' => t('Change "Submitted by" display on all nodes types, except blog and forum.'),
    '#options' => array(
      0 => t('Author, Date & Picture'),
      1 => t('Author & Date'),
      2 => t('Author & Picture'),
      3 => t('Author only'),
      4 => t('Date only'),
    )
  );

// ### SEO settings ###
  $form['seo'] = array(
    '#type' => 'fieldset',
    '#title' => t('Search engine optimization (SEO) settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['seo']['page_h1'] = array(
    '#type' => 'select',
    '#default_value' => theme_get_setting('page_h1'),
    '#description' => t('Change "site name" heading.'),
    '#options' => array(
      0 => t('H1 for site name on frontpage only (SEO optimized)'),
      1 => t('H1 for site name on all pages (Drupal default)'),
    )
  );
  $form['seo']['block_h3'] = array(
    '#type' => 'select',
    '#default_value' => theme_get_setting('block_h3'),
    '#description' => t('Change "block title" heading.'),
    '#options' => array(
      0 => t('No heading for block title (SEO optimized)'),
      1 => t('H3 for block title (Drupal default)'),
    )
  );

// ### Slideshow ###
  $form['slideshow'] = array(
    '#type' => 'fieldset',
    '#title' => t('Slideshow'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['slideshow']['slideshow_display'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show slideshow'),
    '#default_value' => theme_get_setting('slideshow_display'),
    '#description'   => t('Check this option to show Slideshow on the front page.'),
  );
  $form['slideshow']['slideshow_all'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show slideshow on all pages'),
    '#default_value' => theme_get_setting('slideshow_all'),
    '#description'   => t('Check this option to show Slideshow on all pages.'),
  );
  $form['slideshow']['slideimage'] = array(
    '#markup' => t('<sup>To update the slide images, you can select from preloaded images in the "_custom/sliderimg" folder of the theme or from any other folder or server. The <b>Image URL</b> must be formatted as "/themes/contrib/abc/_custom/sliderimg/IMAGE" or "//otherserver.com/IMAGE". The slides are language-aware, and the <b>Slide URL</b> can point to Home (if left blank), internal pages ("node/NODE_NUMBER"), or any webpage on the Internet ("//www.website.com").</sup>'),
  );
  $form['slideshow']['slidety'] = array(
    '#type' => 'select',
    '#title' => t('Slideshow type'),
    '#default_value' => theme_get_setting('slidety'),
    '#options' => array(
      0 => t('Half page image'),
      1 => t('Full page image'),
    )
  );
  $form['slideshow']['slideh'] = array(
    '#type' => 'textfield',
    '#title' => t('Slide height'),
    '#description' => t('Adjust the height of the slide image as needed, using <b>em</b> values if possible (e.g., 25em) or px values (e.g., 400px). You can also leave it empty for full height.'),
    '#default_value' => theme_get_setting('slideh'),
    '#size' => 10,
  );
  $form['slideshow']['slide1'] = array(
    '#type' => 'fieldset',
    '#title' => t('Slide 1'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['slideshow']['slide1']['show_slide1'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show Slide 1'),
    '#default_value' => theme_get_setting('show_slide1'),
  );
  $form['slideshow']['slide1']['slide1_head'] = array(
    '#type' => 'textfield',
    '#title' => t('Slide Headline'),
    '#default_value' => theme_get_setting('slide1_head'),
  );
  $form['slideshow']['slide1']['slide1_desc'] = array(
    '#type' => 'textarea',
    '#title' => t('Slide Description'),
    '#default_value' => theme_get_setting('slide1_desc'),
  );
  $form['slideshow']['slide1']['slide1_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Slide URL'),
    '#default_value' => theme_get_setting('slide1_url'),
  );
  $form['slideshow']['slide1']['slide1_image_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Image URL'),
    '#default_value' => theme_get_setting('slide1_image_url'),
  );
  $form['slideshow']['slide1']['slide1_alt'] = array(
    '#type' => 'textfield',
    '#title' => t('Image Alt Text'),
    '#default_value' => theme_get_setting('slide1_alt'),
  );
  $form['slideshow']['slide2'] = array(
    '#type' => 'fieldset',
    '#title' => t('Slide 2'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['slideshow']['slide2']['show_slide2'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show Slide 2'),
    '#default_value' => theme_get_setting('show_slide2'),
  );
  $form['slideshow']['slide2']['slide2_head'] = array(
    '#type' => 'textfield',
    '#title' => t('Slide Headline'),
    '#default_value' => theme_get_setting('slide2_head'),
  );
  $form['slideshow']['slide2']['slide2_desc'] = array(
    '#type' => 'textarea',
    '#title' => t('Slide Description'),
    '#default_value' => theme_get_setting('slide2_desc'),
  );
  $form['slideshow']['slide2']['slide2_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Slide URL'),
    '#default_value' => theme_get_setting('slide2_url'),
  );
  $form['slideshow']['slide2']['slide2_image_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Image URL'),
    '#default_value' => theme_get_setting('slide2_image_url'),
  );
  $form['slideshow']['slide2']['slide2_alt'] = array(
    '#type' => 'textfield',
    '#title' => t('Image Alt Text'),
    '#default_value' => theme_get_setting('slide2_alt'),
  );
  $form['slideshow']['slide3'] = array(
    '#type' => 'fieldset',
    '#title' => t('Slide 3'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['slideshow']['slide3']['show_slide3'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show Slide 3'),
    '#default_value' => theme_get_setting('show_slide3'),
  );
  $form['slideshow']['slide3']['slide3_head'] = array(
    '#type' => 'textfield',
    '#title' => t('Slide Headline'),
    '#default_value' => theme_get_setting('slide3_head'),
  );
  $form['slideshow']['slide3']['slide3_desc'] = array(
    '#type' => 'textarea',
    '#title' => t('Slide Description'),
    '#default_value' => theme_get_setting('slide3_desc'),
  );
  $form['slideshow']['slide3']['slide3_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Slide URL'),
    '#default_value' => theme_get_setting('slide3_url'),
  );
  $form['slideshow']['slide3']['slide3_image_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Image URL'),
    '#default_value' => theme_get_setting('slide3_image_url'),
  );
  $form['slideshow']['slide3']['slide3_alt'] = array(
    '#type' => 'textfield',
    '#title' => t('Image Alt Text'),
    '#default_value' => theme_get_setting('slide3_alt'),
  );
  $form['slideshow']['slide4'] = array(
    '#type' => 'fieldset',
    '#title' => t('Slide 4'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['slideshow']['slide4']['show_slide4'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show Slide 4'),
    '#default_value' => theme_get_setting('show_slide4'),
  );
  $form['slideshow']['slide4']['slide4_head'] = array(
    '#type' => 'textfield',
    '#title' => t('Slide Headline'),
    '#default_value' => theme_get_setting('slide4_head'),
  );
  $form['slideshow']['slide4']['slide4_desc'] = array(
    '#type' => 'textarea',
    '#title' => t('Slide Description'),
    '#default_value' => theme_get_setting('slide4_desc'),
  );
  $form['slideshow']['slide4']['slide4_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Slide URL'),
    '#default_value' => theme_get_setting('slide4_url'),
  );
  $form['slideshow']['slide4']['slide4_image_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Image URL'),
    '#default_value' => theme_get_setting('slide4_image_url'),
  );
  $form['slideshow']['slide4']['slide4_alt'] = array(
    '#type' => 'textfield',
    '#title' => t('Image Alt Text'),
    '#default_value' => theme_get_setting('slide4_alt'),
  );
  $form['slideshow']['slide5'] = array(
    '#type' => 'fieldset',
    '#title' => t('Slide 5'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['slideshow']['slide5']['show_slide5'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show Slide 5'),
    '#default_value' => theme_get_setting('show_slide5'),
  );
  $form['slideshow']['slide5']['slide5_head'] = array(
    '#type' => 'textfield',
    '#title' => t('Slide Headline'),
    '#default_value' => theme_get_setting('slide5_head'),
  );
  $form['slideshow']['slide5']['slide5_desc'] = array(
    '#type' => 'textarea',
    '#title' => t('Slide Description'),
    '#default_value' => theme_get_setting('slide5_desc'),
  );
  $form['slideshow']['slide5']['slide5_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Slide URL'),
    '#default_value' => theme_get_setting('slide5_url'),
  );
  $form['slideshow']['slide5']['slide5_image_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Image URL'),
    '#default_value' => theme_get_setting('slide5_image_url'),
  );
  $form['slideshow']['slide5']['slide5_alt'] = array(
    '#type' => 'textfield',
    '#title' => t('Image Alt Text'),
    '#default_value' => theme_get_setting('slide5_alt'),
  );
  $form['slideshow']['slide6'] = array(
    '#type' => 'fieldset',
    '#title' => t('Slide 6'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['slideshow']['slide6']['show_slide6'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show Slide 6'),
    '#default_value' => theme_get_setting('show_slide6'),
  );
  $form['slideshow']['slide6']['slide6_head'] = array(
    '#type' => 'textfield',
    '#title' => t('Slide Headline'),
    '#default_value' => theme_get_setting('slide6_head'),
  );
  $form['slideshow']['slide6']['slide6_desc'] = array(
    '#type' => 'textarea',
    '#title' => t('Slide Description'),
    '#default_value' => theme_get_setting('slide6_desc'),
  );
  $form['slideshow']['slide6']['slide6_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Slide URL'),
    '#default_value' => theme_get_setting('slide6_url'),
  );
  $form['slideshow']['slide6']['slide6_image_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Image URL'),
    '#default_value' => theme_get_setting('slide6_image_url'),
  );
  $form['slideshow']['slide6']['slide6_alt'] = array(
    '#type' => 'textfield',
    '#title' => t('Image Alt Text'),
    '#default_value' => theme_get_setting('slide6_alt'),
  );
  $form['slideshow']['slide7'] = array(
    '#type' => 'fieldset',
    '#title' => t('Slide 7'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['slideshow']['slide7']['show_slide7'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show Slide 7'),
    '#default_value' => theme_get_setting('show_slide7'),
  );
  $form['slideshow']['slide7']['slide7_head'] = array(
    '#type' => 'textfield',
    '#title' => t('Slide Headline'),
    '#default_value' => theme_get_setting('slide7_head'),
  );
  $form['slideshow']['slide7']['slide7_desc'] = array(
    '#type' => 'textarea',
    '#title' => t('Slide Description'),
    '#default_value' => theme_get_setting('slide7_desc'),
  );
  $form['slideshow']['slide7']['slide7_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Slide URL'),
    '#default_value' => theme_get_setting('slide7_url'),
  );
  $form['slideshow']['slide7']['slide7_image_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Image URL'),
    '#default_value' => theme_get_setting('slide7_image_url'),
  );
  $form['slideshow']['slide7']['slide7_alt'] = array(
    '#type' => 'textfield',
    '#title' => t('Image Alt Text'),
    '#default_value' => theme_get_setting('slide7_alt'),
  );

// ### Social links ###
  $form['social_links'] = array(
    '#type' => 'fieldset',
    '#title' => t('Social Media Links'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['social_links']['social_links_display'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display social links at the bottom of the page'),
    '#default_value' => theme_get_setting('social_links_display'),
  );
  $form['social_links']['social_links_display_links'] = array(
    '#type' => 'fieldset',
    '#title' => t('Display these social links'),
    '#collapsible' => TRUE,
  );
  $form['social_links']['social_links_display_links']['facebook'] = array(
    '#type' => 'checkbox',
    '#title' => t('Facebook'),
    '#default_value' => theme_get_setting('facebook'),
  );
  $form['social_links']['social_links_display_links']['facebook_link'] = array(
    '#type' => 'textfield',
    '#title' => t('Link to Facebook page'),
    '#description' => t('Enter the link to your Facebook page.'),
    '#size' => 60,
    '#default_value' => theme_get_setting('facebook_link'),
  );
  $form['social_links']['social_links_display_links']['googleplus'] = array(
    '#type' => 'checkbox',
    '#title' => t('Google Plus'),
    '#default_value' => theme_get_setting('googleplus'),
  );
  $form['social_links']['social_links_display_links']['googleplus_link'] = array(
    '#type' => 'textfield',
    '#title' => t('Link to Google Plus page'),
    '#description' => t('Enter the link to your Google Plus account.'),
    '#size' => 60,
    '#default_value' => theme_get_setting('googleplus_link'),
  );
  $form['social_links']['social_links_display_links']['twitter'] = array(
    '#type' => 'checkbox',
    '#title' => t('X (Twitter)'),
    '#default_value' => theme_get_setting('twitter'),
  );
  $form['social_links']['social_links_display_links']['twitter_link'] = array(
    '#type' => 'textfield',
    '#title' => t('Link to Twitter page'),
    '#description' => t('Enter the link to your Twitter account.'),
    '#size' => 60,
    '#default_value' => theme_get_setting('twitter_link'),
  );
  $form['social_links']['social_links_display_links']['instagram'] = array(
    '#type' => 'checkbox',
    '#title' => t('Instagram'),
    '#default_value' => theme_get_setting('instagram'),
  );
  $form['social_links']['social_links_display_links']['instagram_link'] = array(
    '#type' => 'textfield',
    '#title' => t('Link to Instagram page'),
    '#description' => t('Enter the link to your Instagram account.'),
    '#size' => 60,
    '#default_value' => theme_get_setting('instagram_link'),
  );
  $form['social_links']['social_links_display_links']['telegram'] = array(
    '#type' => 'checkbox',
    '#title' => t('telegram'),
    '#default_value' => theme_get_setting('telegram'),
  );
  $form['social_links']['social_links_display_links']['telegram_link'] = array(
    '#type' => 'textfield',
    '#title' => t('Link to telegram page'),
    '#description' => t('Enter the link to your telegram account.'),
    '#size' => 60,
    '#default_value' => theme_get_setting('telegram_link'),
  );
  $form['social_links']['social_links_display_links']['pinterest'] = array(
    '#type' => 'checkbox',
    '#title' => t('Pinterest'),
    '#default_value' => theme_get_setting('pinterest'),
  );
  $form['social_links']['social_links_display_links']['pinterest_link'] = array(
    '#type' => 'textfield',
    '#title' => t('Link to Pinterest page'),
    '#description' => t('Enter the link to your Pinterest account.'),
    '#size' => 60,
    '#default_value' => theme_get_setting('pinterest_link'),
  );
  $form['social_links']['social_links_display_links']['linkedin'] = array(
    '#type' => 'checkbox',
    '#title' => t('LinkedIn'),
    '#default_value' => theme_get_setting('linkedin'),
  );
  $form['social_links']['social_links_display_links']['linkedin_link'] = array(
    '#type' => 'textfield',
    '#title' => t('Link to LinkedIn page'),
    '#description' => t('Enter the link to your LinkedIn account.'),
    '#size' => 60,
    '#default_value' => theme_get_setting('linkedin_link'),
  );
  $form['social_links']['social_links_display_links']['youtube'] = array(
    '#type' => 'checkbox',
    '#title' => t('Youtube'),
    '#default_value' => theme_get_setting('youtube'),
  );
  $form['social_links']['social_links_display_links']['youtube_link'] = array(
    '#type' => 'textfield',
    '#title' => t('Link to Youtube page'),
    '#description' => t('Enter the link to your Youtube account.'),
    '#size' => 60,
    '#default_value' => theme_get_setting('youtube_link'),
  );
  $form['social_links']['social_links_display_links']['vimeo'] = array(
    '#type' => 'checkbox',
    '#title' => t('Vimeo'),
    '#default_value' => theme_get_setting('vimeo'),
  );
  $form['social_links']['social_links_display_links']['vimeo_link'] = array(
    '#type' => 'textfield',
    '#title' => t('Link to Vimeo page'),
    '#description' => t('Enter the link to your Vimeo account.'),
    '#size' => 60,
    '#default_value' => theme_get_setting('vimeo_link'),
  );
  $form['social_links']['social_links_display_links']['flickr'] = array(
    '#type' => 'checkbox',
    '#title' => t('Flickr'),
    '#default_value' => theme_get_setting('flickr'),
  );
  $form['social_links']['social_links_display_links']['flickr_link'] = array(
    '#type' => 'textfield',
    '#title' => t('Link to Flickr page'),
    '#description' => t('Enter the link to your Flickr account.'),
    '#size' => 60,
    '#default_value' => theme_get_setting('flickr_link'),
  );
  $form['social_links']['social_links_display_links']['tumblr'] = array(
    '#type' => 'checkbox',
    '#title' => t('Tumblr'),
    '#default_value' => theme_get_setting('tumblr'),
  );
  $form['social_links']['social_links_display_links']['tumblr_link'] = array(
    '#type' => 'textfield',
    '#title' => t('Link to Tumblr page'),
    '#description' => t('Enter the link to your Tumblr account.'),
    '#size' => 60,
    '#default_value' => theme_get_setting('tumblr_link'),
  );
  $form['social_links']['social_links_display_links']['skype'] = array(
    '#type' => 'checkbox',
    '#title' => t('Skype'),
    '#default_value' => theme_get_setting('skype'),
  );
  $form['social_links']['social_links_display_links']['skype_link'] = array(
    '#type' => 'textfield',
    '#title' => t('Link to Skype page'),
    '#description' => t('Enter the contact link to your Skype account (<b>skype:username?call</b>).'),
    '#size' => 60,
    '#default_value' => theme_get_setting('skype_link'),
  );
  $form['social_links']['social_links_display_links']['drupal'] = array(
    '#type' => 'checkbox',
    '#title' => t('drupal'),
    '#default_value' => theme_get_setting('drupal'),
  );
  $form['social_links']['social_links_display_links']['drupal_link'] = array(
    '#type' => 'textfield',
    '#title' => t('Link to drupal page'),
    '#description' => t('Enter the link to your drupal account.'),
    '#size' => 60,
    '#default_value' => theme_get_setting('drupal_link'),
  );
  $form['social_links']['social_links_display_links']['rss'] = array(
    '#type' => 'checkbox',
    '#title' => t('rss'),
    '#default_value' => theme_get_setting('rss'),
  );
  $form['social_links']['social_links_display_links']['rss_link'] = array(
    '#type' => 'textfield',
    '#title' => t('Link to rss page'),
    '#description' => t('Enter the link to your rss account.'),
    '#size' => 60,
    '#default_value' => theme_get_setting('rss_link'),
  );
  $form['social_links']['social_links_display_links']['myother'] = array(
    '#type' => 'checkbox',
    '#title' => t('Other Social Network'),
    '#default_value' => theme_get_setting('myother'),
  );
  $form['social_links']['social_links_display_links']['myother_link'] = array(
    '#type' => 'textfield',
    '#title' => t('Link to other social network page (custom)'),
    '#description' => t('Enter the link to other social network page.'),
    '#size' => 60,
    '#default_value' => theme_get_setting('myother_link'),
  );

// ### Other settings ###
  $form['themedev'] = array(
    '#type' => 'fieldset',
    '#title' => t('Theme development settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['themedev']['rebuild_registry'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Rebuild theme registry on every page.'),
    '#default_value' => theme_get_setting('rebuild_registry'),
    '#description'   => t('During theme development, it can be very useful to continuously <a href="https://drupal.org/node/173880#theme-registry">rebuild the theme registry</a>. <br /> <div class="alert alert-warning messages warning"><b>WARNING</b>: this is a huge performance penalty and must be turned off on production websites.</div>'),
  );
  $form['themedev']['httpheaders'] = array(
    '#type' => 'checkbox',
    '#title' => t('Remove HTTP Headers'),
    '#default_value' => theme_get_setting('httpheaders'),
    '#description' => t('Remove Drupal 7 Generator meta tag from header.'),
  );
  $form['themedev']['siteid'] = array(
    '#type' => 'textfield',
    '#title' => t('Site ID body class.'),
    '#description' => t('To achieve distinct theme styles in a multisite/multi-theme environment, it can be beneficial to assign a unique "ID" and customize the appearance of each site/theme in the custom-style.css file.'),
    '#default_value' => theme_get_setting('siteid'),
    '#size' => 10,
  );

// Info
  $form['info'] = array(
    '#type' => 'fieldset',
    '#description'   => t('<div class="messages info">Some of the theme settings are <b>multilingual variables</b>. You may have different settings for each language.</div>'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );

}  
