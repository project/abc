<!DOCTYPE html>
<html<?php print $html_attributes . $rdf_namespaces ?>>

<head>
<title><?php print $head_title ?></title>
<meta name="HandheldFriendly" content="true" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<meta name="MobileOptimized" content="width" />
<?php print $head ?>
<?php if (theme_get_setting('fntfam') !== '0'): ?>
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
<?php endif; ?>
<?php print $styles ?>
<?php print $scripts ?>
</head>

<body id="<?php print $body_id ?>" class="<?php print $classes ?>" <?php print $attributes ?>>
  <div id="skip-link">
    <a href="#main" class="element-invisible element-focusable"><?php print t('Skip to main content') ?></a>
    <a href="#search-block-form" class="element-invisible element-focusable"><?php print t('Skip to search') ?></a>
  </div>

<?php print $page_top ?>
<?php print $page ?>
<?php print $page_bottom ?>

<!--[if IE 9]>
<script async src="<?php print $base_path . $path_to_abc ?>/js/classList.min.js"></script>
<![endif]-->
<!--[if gte IE 9]><!-->
<script async src="<?php print $base_path . $path_to_abc ?>/js/toggles.min.js"></script>
<!--<![endif]-->
</body>
</html>